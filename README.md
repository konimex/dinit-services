## dinit services

UNSTABLE! Force pushes will be common until this line is removed/git
repo moved to artix org instead of my personal gitea account

### Rationale

The current situation of service packages are not ideal. Due to Artix's
nature (with support for multiple service managers), we can't just
include the service packages into the package like other service
managers.

So, as a solution, Artix initially added "init" packages, a separate
package that is attached to the main package, but has to be installed
separately. So, say you want to install iwd, you'll need to install
iwd-dinit separately.

Obviously, this is not ideal since the average user probably won't be
aware of it. So I need to find a way to automate these.

There are two alternatives I can think of.

1. wget the service packages on demand.
2. Installing all dinit-services (they're quite small to be quite
   honest), and symlink them to the appropriate directories.
3. Use an Artix-wide converter script from a special format to dinit
   files (Not feasible, since we'll need to coord with other inits, and
   they all have different functionalities)

I went with #2, since #1 requires constant Internet connection.

### Caveats

1. pacman won't track the files
   Since the transaction is done in hooks, not as part of pacman, the
   files in /etc, /usr/lib/dinit/, etc. won't be tracked by pacman.
2. ISO bundling (need clarification)

### Implementation details

#### Hook files

1. dinit-migrate (for migration purposes) - installed in dinit-base
2. dinit-services-{install,remove} (for installations/removal) -
   installed in its own pkg along with service files, dependent to
   dinit-base

#### Migration plan

1. A temporary hook will be installed by the dinit-base pkg, which will only be
   triggered if this package (henceforth referred to as dinit-services) exists.
2. Add dinit-services to the repos, make dinit-base depend on it
3. PreTransaction, the hook will run `pacman -Qq | grep -Ee "-dinit(-user)?$"`
   and save it to a temporary file in `/var` (not `/var/run`) so that
   users are aware of the installed packages.
4. dinit-services will replace all *-dinit packages by using (Replace=),
   removing any and all /etc/dinit.d files which is handled by packages
   (since /etc/dinit.d/boot.d are symlinks, it shouldn't be affected)
5. PostTransaction, dinit-migrate will place all dinit files which was saved
   in that /var directory to /etc/dinit.d (and any other appropriate
   libraries), remove the /var file

#### On new packages/post-migration

dinit-services-install hook will find if there are init services in the /usr/lib
directories. If it exists, it copies/symlinks the file to /etc/dinit.d,
available for starting.

#### On removals

dinit-services-remove hook will simply remove the dinit symlink file.

#### On upgrades

Most files shouldn't be affected as they are symlinks. Further
investigations needed for config files, but we'll end up reimplement
some of pacman (checksumming and if it's modified, put a .pacnew file,
etc).

#### On services handled by the main package

Nothing to do.

#### On user services

Users which uses a user service manager different than their system
service managers (e.g. turnstile users with OpenRC/runit/s6 init
systems) might see dinit files laying in /usr/lib, but ultimately it's
harmless as the file isn't copied unless the hook (in dinit-base
package) is installed.
